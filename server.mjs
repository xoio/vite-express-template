import express from "express"
import path from "path"
import fs from "fs"
import {fileURLToPath} from "url"
import {createServer as createViteServer} from "vite";

const __dirname = path.dirname(fileURLToPath(import.meta.url))

async function createServer() {

    const app = express();

    // Create Vite server in middleware mode and configure the app type as
    // 'custom', disabling Vite's own HTML serving logic so parent server
    // can take control
    const vite = await createViteServer({
        server: {middlewareMode: true},
        appType: "custom",
        logLevel: "info",
        root: "site",
        optimizeDeps: {include: []},
    });

    // use vite's connect instance as middleware
    // if you use your own express router (express.Router()), you should use router.use
    app.use(vite.middlewares);

    // setup static assets directory
    const assetsDir = path.resolve(__dirname, "site", "static")
    const requestHandler = express.static(assetsDir);
    app.use(requestHandler);
    app.use("/static", requestHandler);

    //catch-all for routes and load base template, assuming SPA style for apps
    const template = fs.readFileSync(path.resolve(__dirname, "site", "index.html"), "utf8")
    app.use("*", async (req, res, next) => {
        const url = req.originalUrl

        try {
            const html = await vite.transformIndexHtml(url, template)

            res.status(200).set({
                'Content-Type': 'text/html'
            }).end(html)
        } catch (e) {
            console.log(e)
            next(e)
        }
    })

    app.listen(3000,() => {
        console.log("Starting on port 3000")
    })
}

createServer()
